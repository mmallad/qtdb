//Dipak Malla
//mmallad
#ifndef DB_H
#define DB_H

#include <QtCore/qglobal.h>
#include <QtSql>
#include <QtSql/QSqlDatabase>
#include <QSettings>
#include <QDebug>

#if defined(DB_LIBRARY)
#  define DBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DBSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace Dtsbs
{
    class Db;
}

class DBSHARED_EXPORT Db {
public:
    Db();
    ~Db();
    Db(QString);
    Db(QString host, QString user, QString password, QString database);
    bool Connect();
    QSqlDatabase getConnection();
    void Disconnect();
    void Close();
    QString getConnectionId();
private:
    QString host;
    QString database;
    QString user;
    QString password;
    QString connectionName;
    QSqlDatabase connection;
};

#endif // DB_H
