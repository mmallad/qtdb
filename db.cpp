#include "db.h"
Db::Db()
{
    try
    {
        //Load Database settings from QSettings.
        QSettings settings("DefaultDatabase","ProductName");
        settings.beginGroup("startDatabaseConfig");
        //Try to load host name with default localhost :)
        this->host = settings.value("hostname","localhost").toString();
        this->user = settings.value("user","root").toString();
        this->password = settings.value("password","nopassword").toString();
        this->database = settings.value("database","test").toString();
        settings.endGroup();
        this->connectionName = "A"+QString(qrand());
    }
    catch(QString ex)
    {
        //TODO Implement nice throw :)
        throw ex;
    }
}
Db::Db(QString host, QString user, QString password, QString database)
{
    try
    {
        this->host = host;
        this->user = user;
        this->password = password;
        this->database = database;
        this->connectionName = "A"+QString(qrand());
    }
    catch(QString ex)
    {
        //TODO Implement nice throw :)
        throw ex;
    }
}
QSqlDatabase Db::getConnection()
{
    return this->connection;
}

void Db::Close()
{
    this->connection.removeDatabase(this->connectionName);
}
QString Db::getConnectionId()
{
    return this->connectionName;
}
bool Db::Connect()
{
    try
    {
        //Check for parameters :)
        if(this->database != NULL && this->user != NULL && this->host != NULL)
        {
            this->connection = QSqlDatabase::addDatabase("QMYSQL",this->connectionName);
            this->connection.setHostName(this->host);
            this->connection.setUserName(this->user);
            this->connection.setPassword(this->password);
            this->connection.setDatabaseName(this->database);
            if(this->connection.open())
                return true;
            else
                throw this->connection.lastError().text();
        }
        else
        {
            throw "Invalid Parameters.";
        }
    }
    catch(QString ex)
    {
        //TODO Implement nice throw :)
        throw ex;
    }
}
Db::~Db()
{

}
