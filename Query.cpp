#include "Query.h"

void Query::Init()
{
    try
    {
        this->tranStarted = false;
        this->Connect();
    }
    catch(QString ex)
    {
        throw ex;
    }
}
Query::Query():Db()
{
    this->Init();
}
Query::Query(QString host, QString user, QString password, QString database):Db(host,user,password,database)
{
    this->Init();
}
Query * Query::setTableName(QString tableName)
{
    this->tableName = tableName;
    return this;
}
Query * Query::setColumnName(QHash<int, QString> cols)
{
    this->cols = cols;
}
Query * Query::setValues(QHash<QString, QString> vals)
{
    this->vals = vals;
}
Query * Query::setWhere(QHash<QString, QString> where)
{
    this->where = where;
}
void Query::Select()
{
    try
    {
        this->SQBuilder();
        this->Execute();
    }
    catch(QString ex)
    {
        throw ex;
    }

}
void Query::Update()
{
    try
    {
        this->UQBuilder();
        this->Execute();
    }
    catch(QString ex)
    {
        throw ex;
    }
}

void Query::Delete()
{
    try
    {
        this->DQBuilder();
        this->Execute();
    }
    catch(QString ex)
    {
        throw ex;
    }
}

void Query::Insert()
{
    try
    {
        this->IQBuilder();
        this->Execute();
    }
    catch(QString ex)
    {
        throw ex;
    }
}
int Query::rowCount()
{
    return this->affectedRows;
}
QSqlQuery Query::getQuery()
{
    return this->query;
}
void Query::Execute()
{
    //qDebug() << this->sqlQuery;
    try
    {
        if(!this->tranStarted)
            this->getConnection().database(this->getConnectionId()).transaction();
        else
            this->tranStarted = true;
        this->query.prepare(this->sqlQuery);
        if(this->sqlValues.count() >= 1)
        {
            foreach(int i, this->sqlValues.keys())
            {
                this->query.addBindValue(this->sqlValues[i]);
            }
        }
        this->query.exec();
        this->affectedRows = this->query.numRowsAffected();
    }
    catch(QString ex)
    {
        this->getConnection().database(this->getConnectionId()).rollback();
        throw ex;
    }
}
void Query::UQBuilder()
{
    this->sqlValues.clear();
    this->sqlQuery = "UPDATE "+this->tableName+" ";
    QString tmp = "";
    QHashIterator<QString, QString> v(this->vals);
    int i = 0;
    while(v.hasNext())
    {
        v.next();
        tmp += v.key()+"=?,";
        this->sqlValues.insert(i++,v.value());
    }
    tmp.remove(QRegExp("[,]$"));
    this->sqlQuery += "SET "+tmp;
    if(this->where.count() <= 0)
        return;
    tmp = "";
    QHashIterator<QString, QString> w(this->where);
    i = 0;
    while(w.hasNext())
    {
        w.next();
        tmp += "" + w.key() + "=? AND ";
        this->sqlValues.insert(i++,w.value());
    }
    tmp.remove(QRegExp("[AND ]{4}$"));
    this->sqlQuery += " WHERE "+tmp;
}
void Query::Save()
{
    this->getConnection().database(this->getConnectionId()).commit();
}
void Query::Cancel()
{
    this->getConnection().database(this->getConnectionId()).rollback();
}
void Query::DQBuilder()
{
    this->sqlValues.clear();
    this->sqlQuery = "DELETE FROM "+this->tableName;
    if(this->where.count() <= 0)
        return;
    QString tmp = "";
    QHashIterator<QString, QString> w(this->where);
    int i = 0;
    while(w.hasNext())
    {
        w.next();
        tmp += "" + w.key() + "=? AND ";
        this->sqlValues.insert(i++,w.value());
    }
    tmp.remove(QRegExp("[AND ]{4}$"));
    this->sqlQuery += " WHERE "+tmp;
}

void Query::IQBuilder()
{
    this->sqlValues.clear();
    this->sqlQuery = "INSERT INTO "+this->tableName+" ";
    QString tmp = "";
    foreach (int i, this->cols.keys()) {
        tmp += this->cols[i]+",";
    }
    tmp.remove(QRegExp("[,]$"));
    this->sqlQuery += "("+tmp+")";
    tmp = "";
    QHashIterator<QString, QString> v(this->vals);
    int i = 0;
    while(v.hasNext())
    {
        v.next();
        tmp += "?,";
        this->sqlValues.insert(i++,v.value());
    }
    tmp.remove(QRegExp("[,]$"));
    this->sqlQuery += " VALUES("+tmp+")";
}
void Query::SQBuilder()
{
    this->sqlValues.clear();
    this->sqlQuery = "SELECT ";
    QString tmp = "";
    foreach (int i, this->cols.keys()) {
        tmp += this->cols[i]+",";
    }
    tmp.remove(QRegExp("[,]$"));
    this->sqlQuery += tmp + " FROM "+this->tableName;
    if(this->where.count() <= 0)
        return;
    tmp = "";
    QHashIterator<QString, QString> w(this->where);
    int i = 0;
    while(w.hasNext())
    {
        w.next();
        tmp += "" + w.key() + "=? AND ";
        this->sqlValues.insert(i++,w.value());
    }
    tmp.remove(QRegExp("[AND ]{4}$"));
    this->sqlQuery += " WHERE "+tmp;
}
Query::~Query()
{
    //TODO Clear
    this->Close();
}
