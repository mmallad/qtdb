//Dpak Malla
//mmallad
#ifndef QUERY_H
#define QUERY_H

#include "db.h"

#if defined(QUERY_LIBRARY)
#  define QUERYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define QUERYSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace Dtsbs
{
    class Query;
}
class QUERYSHARED_EXPORT Query : public Db
{
    public:
        Query();
        Query(QString host, QString user, QString password, QString database);
        ~Query();
        void Insert();
        void Update();
        void Select();
        void Delete();
        int rowCount();
        QSqlQuery getQuery();
        Query * setTableName(QString);
        Query * setColumnName(QHash<int,QString>);
        Query * setValues(QHash<QString,QString>);
        Query * setWhere(QHash<QString,QString>);
    private:
        QString tableName;
        QHash<int,QString> cols;
        QHash<QString,QString> vals;
        QHash<QString,QString> where;
        void SQBuilder();
        void IQBuilder();
        void DQBuilder();
        void UQBuilder();
        void Execute();
        bool tranStarted;
        void Save();
        void Cancel();
        QString sqlQuery;
        QHash<int, QString> sqlValues;
        void Init();
        QSqlQuery query;
        int affectedRows;

};

#endif // QUERY_H
